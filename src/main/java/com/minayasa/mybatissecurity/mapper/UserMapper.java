package com.minayasa.mybatissecurity.mapper;

import com.minayasa.mybatissecurity.model.User;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.SelectKey;

@Mapper
public interface UserMapper {
    @Insert("insert into users(first_name, last_name, email, password, username) values (#{firstName}, #{lastName}, #{email}, #{password}, #{username})")
    @SelectKey(statement = "SELECT LAST_INSERT_ID()", keyProperty = "id", before = false, resultType = Integer.class)
    int register(User user);

    @Select("select * from users where username = #{username} or email= #{username}")
    User findByUsername(String username);
}
