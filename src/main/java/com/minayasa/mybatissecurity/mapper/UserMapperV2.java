package com.minayasa.mybatissecurity.mapper;

import com.minayasa.mybatissecurity.entities.UserRegisterEntity;
import com.minayasa.mybatissecurity.util.MyBatisUtil;
import org.apache.ibatis.session.SqlSession;
import org.springframework.stereotype.Repository;

@Repository
public class UserMapperV2 {
    public void addUser(UserRegisterEntity user) {
        SqlSession session = MyBatisUtil.getSessionFactory().openSession();
        session.insert("insertUser", user);
        session.commit();
        session.close();
    }

    public UserRegisterEntity findUserByEmail(UserRegisterEntity user){
        SqlSession session = MyBatisUtil.getSessionFactory().openSession();
        UserRegisterEntity users = session.selectOne("findUserByEmail", user);
        session.commit();
        session.close();
        return users;
    }

    public UserRegisterEntity findUserByEmailOrPhone(UserRegisterEntity user) {
        SqlSession session = MyBatisUtil.getSessionFactory().openSession();
        UserRegisterEntity users = session.selectOne("findByEmailOrPhone", user);
        session.commit();
        session.close();
        return users;
    }
}
