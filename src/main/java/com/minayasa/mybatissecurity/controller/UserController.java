package com.minayasa.mybatissecurity.controller;

import com.minayasa.mybatissecurity.config.JwtTokenUtil;
import com.minayasa.mybatissecurity.entities.UserRegisterEntity;
import com.minayasa.mybatissecurity.mapper.UserMapper;
import com.minayasa.mybatissecurity.model.User;
import com.minayasa.mybatissecurity.model.request.JwtRequest;
import com.minayasa.mybatissecurity.service.JwtUserDetailsService;
import com.minayasa.mybatissecurity.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.DisabledException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.*;

import javax.annotation.PostConstruct;
import javax.persistence.ManyToOne;
import java.util.HashMap;
import java.util.Map;

@RestController
@RequestMapping("/api")
public class UserController {

    private UserMapper userMapper;

    @Autowired
    UserService userService;

    @Autowired
    private AuthenticationManager authenticationManager;

    @Autowired
    private JwtTokenUtil jwtTokenUtil;

    @Autowired
    private JwtUserDetailsService userDetailsService;

    @Autowired
    PasswordEncoder passwordEncoder;



    public UserController(UserMapper userMapper) {
        this.userMapper = userMapper;
    }

    @PostMapping("/register")
    public User register(@RequestBody User user) {
        System.out.println(user.getPassword());
        user.setPassword(passwordEncoder.encode(user.getPassword()));
        int insertUser = userMapper.register(user);
        System.out.println("Insert user result " + insertUser);

        return user;
    }

    @PostMapping("/register/v2")
    public ResponseEntity<?> addUser(@RequestBody UserRegisterEntity param) {
        if (userService.emailExist(param)) {
            try {
                param.setPassword(passwordEncoder.encode(param.getPassword()));
                userService.addUser(param);
                System.out.println("Register Success");
                return new ResponseEntity<>(param, HttpStatus.OK);
            } catch (Exception e) {
                System.out.println(e);
                return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
            }
        } return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
    }


    @PostMapping("/login")
    public ResponseEntity<?> createAuthenticationToken(@RequestBody JwtRequest authenticationRequest) throws Exception {

        User userExist = userMapper.findByUsername(authenticationRequest.getUsername());

        if (userExist == null) {
            Map<String, String> erroResponse = new HashMap<>();
            erroResponse.put("message", "User not found");
            return new ResponseEntity<>(erroResponse, HttpStatus.NOT_FOUND);
        }

        authenticate(authenticationRequest.getUsername(), authenticationRequest.getPassword());

        final UserDetails userDetails = userDetailsService
                .loadUserByUsername(authenticationRequest.getUsername());



        final String token = jwtTokenUtil.generateToken(userDetails);

        Map<String, String> userInfo = new HashMap<>();
        userInfo.put("username", authenticationRequest.getUsername());
        userInfo.put("token", token);

        return new ResponseEntity<>(userInfo, HttpStatus.OK);
    }


    @PostMapping("/login/v2")
    public ResponseEntity<?> loginAuth(@RequestBody UserRegisterEntity request) throws Exception {
        authenticate(request.getUsername(), request.getPassword());

        final UserDetails userDetails = userDetailsService
                .loadUserByUsername(request.getUsername());



        final String token = jwtTokenUtil.generateToken(userDetails);

        Map<String, String> userInfo = new HashMap<>();
        userInfo.put("username", request.getUsername());
        userInfo.put("token", token);

        return new ResponseEntity<>(userInfo, HttpStatus.OK);
    }

    private void authenticate(String username, String password) throws Exception {
        try {
            authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(username, password));
        } catch (DisabledException e) {
            throw new Exception("USER_DISABLED", e);
        } catch (BadCredentialsException e) {
            throw new Exception("INVALID_CREDENTIALS", e);
        }
    }

}
