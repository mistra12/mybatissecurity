package com.minayasa.mybatissecurity.service;

import com.minayasa.mybatissecurity.entities.UserRegisterEntity;

public interface UserService {
    void addUser(UserRegisterEntity param);
    public boolean emailExist(UserRegisterEntity user);
    UserRegisterEntity findUserByEmail(UserRegisterEntity param);
    UserRegisterEntity findUserByEmailOrPhone(UserRegisterEntity param);
}
