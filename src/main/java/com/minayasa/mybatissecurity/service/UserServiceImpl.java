package com.minayasa.mybatissecurity.service;

import com.minayasa.mybatissecurity.entities.UserRegisterEntity;
import com.minayasa.mybatissecurity.mapper.UserMapperV2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.sql.Timestamp;
import java.util.Date;

@Service
public class UserServiceImpl implements UserService{

    @Autowired
    UserMapperV2 userMapper;

    public UserServiceImpl() throws IOException {
    }

    @Override
    public UserRegisterEntity findUserByEmail(UserRegisterEntity param) {
        return userMapper.findUserByEmail(param);
    }

    @Override
    public boolean emailExist(UserRegisterEntity user){
        try {
            userMapper.findUserByEmail(user).getEmail();
            System.out.println("Email Sudah ada");
            return false;
        }catch (Exception e){
            System.out.println("Email Belum terdaftar");
            return true;
        }
    }

    @Override
    public void addUser(UserRegisterEntity param){
        Date date = new Date();
        Timestamp timestamp = new Timestamp(date.getTime());
        param.setCreate_date(timestamp);
        param.setUpdate_date(timestamp);
        userMapper.addUser(param);
    }

    @Override
    public UserRegisterEntity findUserByEmailOrPhone(UserRegisterEntity user) {
        return userMapper.findUserByEmailOrPhone(user);
    }

}
