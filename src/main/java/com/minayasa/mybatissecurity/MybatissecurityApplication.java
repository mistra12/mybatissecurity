package com.minayasa.mybatissecurity;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MybatissecurityApplication {

	public static void main(String[] args) {
		SpringApplication.run(MybatissecurityApplication.class, args);
	}

}
